# secure_crt_session_creator.py
#### Creates SecureCRT sessions based on your Solarwinds NPM devices

##### Requirements:
  - [Python 3.6]
  - NOTE: you must check the "add path variable" box during installation!
  - [SecureCRT]
  - OrionSDK module
  - A Solarwinds NPM Installation

[Python 3.6]: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe
[SecureCRT]: https://www.vandyke.com/products/securecrt/
