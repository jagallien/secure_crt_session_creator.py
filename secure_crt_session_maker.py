#!/usr/bin/python3

###################
#
# SNMP Device Inventory Script
# Queries SolarWinds for a a list of SNMP monitored devices, sorts by location
# by: J.A.Gallien, (j.a.gallien@gmail.com)
#
###################

from datetime import date # Used for naming our file w/ the current date.
import orionsdk
import json
import sys

output_file = open((date.today().strftime('%Y%m%d') + '_secureCRT_sessions.xml'), mode='w')
connector = orionsdk.SwisClient(sys.argv[2], sys.argv[2], sys.argv[3])
result = connector.query("SELECT TOP 400 NodeID, IPAddress, SysName, Location FROM Orion.Nodes Where Status NOT LIKE '9'")
result = (result['results'])
check_list = []

json_output = json.loads(json.dumps(result))

output_file.write("""<?xml version="2.0" encoding="UTF-8"?>
<VanDyke version="3.0">
<key name="Sessions">""")


for i in json_output:
    if i['Location'] in check_list:
        continue
    else:
        checker = connector.query("SELECT TOP 400 NodeID, IPAddress, SysName, Location FROM Orion.Nodes Where Location='" + i['Location'] + "'")
        check_list += i['Location']
        output_file.write('''<key name="''' + i['Location'] + '''">''')
        for k in json.loads(json.dumps(checker['results'])):
            output_file.write('''<key name="''' + k['SysName'] + '''">
            <dword name="[SSH2] Port">22</dword>
            <dword name="Allow Connection Sharing">0</dword>
            <string name="Host Key Algorithms">ssh-rsa,ssh-ed25519,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,null,x509v3-sign-rsa,x509v3-ssh-rsa,x509v3-sign-dss,x509v3-ssh-dss,ssh-dss</string>
            <string name="Hostname">''' + k['IPAddress'] + '''</string>
            <string name="Key Exchange Algorithms">ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha256,diffie-hellman-group-exchange-sha1,gss-group1-sha1-toWM5Slw5Ew8Mqkay+al2g==,gss-gex-sha1-toWM5Slw5Ew8Mqkay+al2g==</string>
            <string name="MAC List">hmac-sha2-512,hmac-sha2-256,hmac-sha1,hmac-sha1-96,hmac-md5,hmac-md5-96,umac-64@openssh.com</string>
            <string name="SSH2 Authentications V2">password,publickey,keyboard-interactive,gssapi</string>
            </key>''')
        output_file.write('</key>')

output_file.write("""</key>
</VanDyke>""")
